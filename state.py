#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 19:08:21 2019

@author: timzim
"""

import numpy as np

class State(object):
    """Stores the state of a ship.
    
    Variables
    ---------
    lat : float
        Current latitude.
    lon : float
        Current longitude.
    orientation : float
        Current orientation in radians.
    linearAccel : float
        Current linear acceleration in m/s^2.
    linearVel : float
        Current linear velocity in m/s.
    angularAccel : float
        Current angular acceleration in r/s^2
    angularVelocity : float
        Current angular velocity in r/s.
    """
    def __init__(self):
        
        self.position     = np.array([[ 0.0 ], 
                                      [ 0.0 ]])
        self.orientation  = 0.0
        
        self.linearAccel  = 0.0
        self.linearVel    = 0.0
        self.linearVelAvg = 0.0
        
        self.angularAccel  = 0.0
        self.angularVel    = 0.0
        self.angularVelAvg = 0.0
        
    def __str__(self):
        return "linearAccel: %+.2f m/s^2   linearVel: %+.2f m/s   angularAccel: %+.2f rad/s^2   angularVel: %+.2f m/s" % (self.linearAccel, self.linearVel, self.angularAccel, self.angularVel)
        
        