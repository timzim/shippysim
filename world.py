#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 20:47:51 2019

@author: timzim
"""

import numpy as np
from ship import Ship
import matplotlib.pyplot as plt

class World:
    """Defines the world that the ship travels in. 
    
    Contains all of the methods required for updating the ship's location
    within the world. Also keeps track of the world time. 
    
    The world is written as a class so worlds can be automatically instantiated 
    and destroyed at-will by the simulator, enabling consecutive simulations to 
    be automatically executed (i.e., for spaghetti plots). 
    
    NOTE: The code is currently written for only one ship.
    """
    
    #TODO: Sun, weather (clouds, wind, waves, currents)
    
    def __init__(self):
        self.shippy          = Ship()
        self.worldTime       = 0.0
        self.dt              = 1.0
        self.breadcrumbs     = {'x':[], 'y':[]}
        self.M_REFLECT_YAXIS = np.array([[1,0],[0,-1]])
        
    def calculateDeltaShipPosition(self, state):
        # Calculate the new position of the ship after dt.
        if state.angularVelAvg == 0:
            deltaX = state.linearVelAvg * self.dt
            deltaY = 0.0
        else:
            # Calculate the radius of the turn with forward and angular velocity
            r = state.linearVelAvg / state.angularVelAvg
            # Consider the unit circle. Boat is traveling along perimeter. This
            # requires subtraction of 1.0 to calculate deltaY. Equations are simply
            # those used for polar coordinates. 
            deltaX = r * np.sin(state.angularVelAvg * self.dt)
            deltaY = r * (1. - np.cos(state.angularVelAvg * self.dt))
            # cos(theta) results in a positive result for turning in either direction.
            # Convert value to negative if angularVelocity is in negative direction.
            if state.angularVelAvg  < 0.: 
                deltaY = np.negative(deltaY)
       
        # Local coordinate system must be rotated to the current ship orientation.
        # https://cs184.eecs.berkeley.edu/uploads/lectures/04_transforms-1/04_transforms-1_slides.pdf
        R_theta = np.array([[np.cos(state.orientation), -np.sin(state.orientation)],
                            [np.sin(state.orientation), np.cos(state.orientation)]])
        deltaPos = np.array([[deltaX],[deltaY]])
        latlonConversion = np.array( [[111132.],
                                      [111320.*np.cos(state.position[0][0])]] )
        newPos = state.position + (self.M_REFLECT_YAXIS @ R_theta @ deltaPos) / latlonConversion
        
        # Update the state with the new position and orientation
        state.position = newPos
        state.orientation = state.orientation + state.angularVelAvg * self.dt
        
        # This is where we need to apply weather, wave, and ocean forces. 
        # Or define a "cleanup" function to fix boat angle range after all
        # forces have been applied.
        
        # An ingenious method from UTSeaSim for fixing angles outside of the 
        # range of -180 < orientation < 180.
        while( state.orientation > 3.14159 ):
            state.orientation -= 6.28319
        while( state.orientation <= -3.14159 ):
            state.orientation += 6.28319
            
        self.breadcrumbs['y'].append(newPos[0][0])
        self.breadcrumbs['x'].append(newPos[1][0])
            
        #print("%f,%f" % (newPos[0][0],newPos[1][0]))
        
    def increment(self):
        """Updates the time and updates the ship state. 
        """
        # Update the world time for the current time step.
        self.worldTime = self.worldTime + self.dt
        # Update the ship state for the current step.
        self.shippy.updateState(self.dt)
        # Use the ship state to calculate the new position.
        self.calculateDeltaShipPosition(self.shippy.state)
    
if __name__ == "__main__":
    
    world = World()
    
    # Set the initial position.
    #world.shippy.setLatLon(38.801682,-75.090917)
    world.shippy.setLatLon(38.,-75.)
    world.shippy.setHeading(90.)
    
    world.shippy.setEngineSpeed(100.)
    
    while world.worldTime < 10000.:
        world.increment()
        
    world.shippy.setRudderAngle(0.01)
    for i in range(0,3):
        world.increment()
    world.shippy.setRudderAngle(0.0)
    
    while world.worldTime < 200000.:
        world.increment()

#    while world.shippy.getLon() <= -74.:
#        world.increment()
#        #print(world.shippy.state.position)
#    print(world.worldTime)
    
    fig,axis = plt.scatter(world.breadcrumbs['x'],world.breadcrumbs['y'], marker='.')
    axis.set_aspect('equal', 'box')
    axis.ylim(0.0,1.0)
