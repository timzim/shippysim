#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 19:40:11 2019

@author: timzim
"""

import random
import numpy as np
from state import State

#TODO: Update docs to match changes.

class Ship(object):
    """Defines a ship and all of its simulator functions.
    
    The physics presented in this class are very
    elementary and do not properly capture the true physical interactions 
    experienced by a boat. However, for the purposes of general analysis of
    the difficulties of battling mother nature to cross the Atlantic with a
    miniature boat, this should suffice. 
    
    The local coordinate system of the boat assumes the bow=+X, stern=-X,
    port=+Y, and starboard=-Y. 
    """
    
    def __init__(self):
        """Initializes the Ship object.
        
        Constants
        ---------
        LENGTH : float
            Defines the total length of the boat in meters.
        MASS : float
            Defines the mass of the boat in kilograms.
        RUDDER_MOMENT_ARM : float
            Defines the distance between the center of gravity of the boat
            about the z-axis to the rudder force vector. Current implementation
            assumes the moment is in the center of the boat, and the rudder is
            located exactly at the stern.
        RUDDLE_ANGLE_LIMIT : float
            Defines the maximum angle the rudder can rotate to in both directions.
        RUDDER_FORCE_CONST : float
            Proportional constant used to calculate the resulting force produced
            by the rudder at the current angle and boat speed.
        ENGINE_SPEED_CONST : float
            Proportional constant used to calculate the engine force based on
            the current engine speed.
        FORWARD_DRAG_CONST : float
            Proportional constant used to calculate the drag on the boat due
            to forward motion.
        ANGULAR_DRAG_CONST : float
            Proportional constant used to calculate the drag on the boat due
            to rotational motion. 
        MOMENT_OF_INTERTIA : float
            Defines the moment of intertia of the boat. This is a static value
            since it should not change. 
            
        Variables
        ---------
        engineSpeed : float
            Stores the current engine speed.
        rudderAngle : float
            Stores the current rudder angle.
        """
        
        self.LENGTH             = 1.0
        self.MASS               = 3.0
        self.RUDDER_MOMENT_ARM  = self.LENGTH * 0.5
        self.RUDDER_ANGLE_LIMIT = 0.785 # rad
        self.RUDDER_FORCE_CONST = 0.1
        self.ENGINE_SPEED_CONST = 0.1
        self.FORWARD_DRAG_CONST = 10.0
        self.ANGULAR_DRAG_CONST = 0.1
        
        #TODO: Validate the calculation of this value.
        self.MOMENT_OF_INERTIA = (1.0 / 12.0) * self.MASS * self.RUDDER_MOMENT_ARM**2
        
        self.engineSpeed = 0.0
        self.rudderAngle = 0.0
        
        # Keeps track of the ship state. 
        self.state = State()
        
    def getHeading(self):
        o = np.degrees(self.state.orientation)
        if o > 0.0:
            return 360.0 - o
        else:
            return -1.0 * o
        
    def setHeading(self,h):
        if h >= 180.:
            self.state.orientation = np.radians(360.0 - h)
        else:
            self.state.orientation = np.radians(-1.0 * h)
            
    def getLat(self):
        return self.state.position[0]
    
    def getLon(self):
        return self.state.position[1]
        
    def setLatLon(self, lat, lon):
        self.state.position[0] = lat
        self.state.position[1] = lon
        
    def setEngineSpeed(self, speed):
        """Sets the engine speed.
        
        Parameters
        ----------
        speed : float
            Speed of the engine as a ratio of the maximum speed. Speed is clipped
            if smaller than -1.0 and larger than 1.0.
        """
        self.engineSpeed = np.clip(speed, -1.0, 1.0)
    
    def calcEngineForce(self):
        """Calculates the force resulting from the engine speed.
        
        The engine force will always be in the +X direction. The resulting
        force is considered to be linear, and is proportional to the constant
        value ENGINE_SPEED_CONST. Update this function if the force is found
        to be non-linear. 
        
        Returns
        -------
        float
            Magnitude of engine force in the forward direction.
        """
        return self.engineSpeed * self.ENGINE_SPEED_CONST
    
    def calcForwardDrag(self, v):
        """Calculates the drag on the boat from forward motion.
        
        Like the engine force, the drag is considered to be linear, and is
        proportional to the square of the velocity. 
        
        Parameters
        ----------
        state : State object
            Reference to the state object which contains the forwardVelocity.
            
        Returns
        -------
        float
            Magnitude and direction of the drag in newtons.
        """
        return -1.0 * v * np.abs(v) * self.FORWARD_DRAG_CONST
    
    def calcForwardAcceleration(self, f):
        """Calculates the forward acceleration of the boat.
        
        Parameters
        ----------
        f : float
            Magnitude of the forward force in newtons.
            
        Returns
        -------
        float
            Acceleration of the boat in the forward direction in :math:`m/s^2`.
        """
        return f / self.MASS
    
    def calcForwardVelocity(self, v0, a, dt):
        """Calculates the forward speed of the boat.
        
        This is a conglomeration of two equations:
        
        .. math:: v = v_0 + at
        
        and
        
        .. math:: \overline{v} = (v + v_0) / 2.0
            
        Parameters
        ----------
        v0 : float
            Initial velocity in m/s.
        a : float
            Forward acceleration in m/s^2.
        dt : float
            Length of the time step in seconds.
        """
        v = v0 + a * dt
        return v, (v+v0)*0.5
    
    def setRudderAngle(self, angle):
        """Sets the rudder angle. 
        
        Clips the requested angle to RUDDER_ANGLE_LIMIT
        if the commanded angle is larger than the maximum deflection.
        
        Parameters
        ----------
        angle : float
            Angle of the rudder in radians.
        """
        self.rudderAngle = np.clip(angle, np.negative(self.RUDDER_ANGLE_LIMIT), self.RUDDER_ANGLE_LIMIT)
    
    def calcRudderForceVector(self, forwardVelocity):
        """Calculates the force vector resulting from the rudder angle. 
        
        Technically this is a torque, but the model assumes the resulting rudder
        force can only occur perpendicular to the centerline at the stern (i.e., 
        along the +/-Y axis). 
        
        Returns
        -------
        float
            Magnitude of the rudder force in newtons. 
        """
        return np.sin(self.rudderAngle) * forwardVelocity * self.RUDDER_FORCE_CONST
        
    def calcAngularDragVector(self, angularVelocity):
        """Calculates the drag caused by rotation of the boat. 
        
        Parameters
        ----------
        angularVelocity : float
            Angular velocity vector.
        
        Returns
        -------
        float
            Angular drag vector in newtons.
        """
        return -1.0 * angularVelocity * np.abs(angularVelocity) * self.ANGULAR_DRAG_CONST
    
    def calcAngularAcceleration(self, rudderForce):
        """Calculates the angular acceleration of the ship using the rudder force 
        and moment of intertia.
        
        The moment of intertia will likely need to be updated once a true ship
        is constructed. UTSeaSim does NOT square the radius.
        
        Parameters
        ----------
        rudderForce : float
            Calculated force created by the rudder.
        """
        return rudderForce / (self.MOMENT_OF_INERTIA)
    
    def calcAngularAvgVelocity(self, stateAngularVel, angularAccel, dt):
        """Calculates the average angular velocity over the timestep.
        
        Parameters
        ----------
        stateAngularVel : float
            The angular velocity at time :math:`t_0`.
        angularAccel : float
            The angular acceleration at :math:`t_0+dt`.
        dt : float
            Length of the timestep. 
            
        Returns
        -------
        float
            Average angular velocity vector in :math:`radians/s`.
        """
        w = stateAngularVel + (angularAccel * dt)
        if w < 1.0e-3: 
            w = 0.0
        return w, (w+stateAngularVel)*0.5
    
    def updateState(self, dt):
        """Calculates the new state of the ship after dt.
        
        Parameters
        ----------
        state : State object
            Reference to the state object.
        dt : float
            Length of the timestep in seconds. 
        """
        forwardForce        = self.calcEngineForce() + self.calcForwardDrag(self.state.linearVel)
        forwardAcceleration = self.calcForwardAcceleration(forwardForce)
        forwardVelocity, forwardAvgVelocity = self.calcForwardVelocity(self.state.linearVel, forwardAcceleration, dt)
        
        rudderForce         = self.calcRudderForceVector(forwardAvgVelocity) + self.calcAngularDragVector(self.state.angularVel)
        angularAccel        = self.calcAngularAcceleration(rudderForce)
        angularVelocity, angularAvgVelocity  = self.calcAngularAvgVelocity(self.state.angularVel, angularAccel, dt)
            
        self.state.linearAccel   = forwardAcceleration
        self.state.linearVel     = forwardVelocity
        self.state.linearVelAvg  = forwardAvgVelocity
        self.state.angularAccel  = angularAccel
        self.state.angularVel    = angularVelocity
        self.state.angularVelAvg = angularAvgVelocity
        
            
if __name__ == "__main__":
    
    # Unit tests go here.    
    ship = Ship()
    dt = 1.0
    ship.setEngineSpeed(100.)
    
    for i in range(1, 20):
        ship.updateState(dt)
        #print("%0.2f" % s.linearVel)
        print(ship.state)
        
    ship.setEngineSpeed(0.)
    
    while ship.state.linearVel > 0.01:
        ship.updateState(dt)
        #print("%0.2f" % s.linearVel)
        print(ship.state)
        